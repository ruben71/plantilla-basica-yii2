<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
?>

<div class="col-sm-4 col-md-3 flex-grow">
    <div class="thumbnail">
      <div class="caption">
  
        <ul>
            <li>Id <?= $modelo->id; ?></li>
            <li>Enlace <?= Html::a($modelo->nombre,$modelo->url); ?></li>
            <?php
                Modal::begin([
                    'header' => '<h2>Descripcion larga</h2>',
                    'toggleButton' => ['tag'=>'li','label' => $modelo->descorta,'class'=>'btn btn-primary'],
                    'footer'=> Html::a('Cerrar',['#'],['class'=>'btn btn-danger btn-sm pull-right','data-dismiss'=>'modal'])
                ]);
            
                echo $modelo->deslarga;
                Modal::end();
            ?>
            
        </ul>

       
      </div>
    </div>
  </div>
